'use strict'

export const USERNAME_SELECTOR = '#email'
export const PASSWORD_SELECTOR = '#pass'
export const BUTTON_SELECTOR = '#loginbutton'

export const POST_TEXTAREA_SELECTOR = 'form > div.clearfix._ikh > div._4bl9 > div > textarea'
export const POST_BUTTON_SELECTOR = 'div._1j2v > div._2dck._4-u3._57d8 > div > div._ohf.rfloat > div > div._332r > button'

export const privatePageURL = 'https://www.facebook.com/notifications'
export const pageLoginURL = 'https://www.facebook.com/login.php'
export const mainPageURL = 'https://www.facebook.com/'
export const groopHalfURL = 'https://www.facebook.com/groups'
