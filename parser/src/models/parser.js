'use strict'

import logger from './logger'

import puppeteer from 'puppeteer'
import { delay } from './worker'

import * as selector from './selector'
import config from '../config/creds'

let myCookies = null
class Facebook {
    constructor(user, pass) {
        if (!user || !pass)
            return new Error('username or password emty')
        this.user = user
        this.pass = pass
        this.browser = null
        this.page = null
    }
    async login() {
        try {
            const parserOption = { args: ['--no-sandbox', '--disable-setuid-sandbox'] }
            if (config.status == 'dev')
                parserOption.headless = false

            this.browser = await puppeteer.launch(parserOption)

            //Enable Facebook Notification
            const context = this.browser.defaultBrowserContext()
            await context.overridePermissions(selector.mainPageURL, ['notifications'])

            this.page = await this.browser.newPage();
            await this.page.setViewport({ width: 1280, height: 800 })

            await this.page.goto(selector.pageLoginURL)
            await this.page.click(selector.USERNAME_SELECTOR);
            await this.page.keyboard.type(this.user)

            await this.page.click(selector.PASSWORD_SELECTOR);
            await this.page.keyboard.type(this.pass)

            await this.page.click(selector.BUTTON_SELECTOR)

            await this.page.waitForNavigation()
            const pageUrl = this.page.url()
            if (pageUrl == selector.mainPageURL) {
                myCookies = await this.page.cookies(selector.privatePageURL)
                // this.browser.close()
                return true
            }
            console.log(pageUrl, selector.mainPageURL)
            console.log(pageUrl === selector.mainPageURL)
            return false

        } catch (err) {
            console.log(`Login error ${err}`)
            return false
        }
    }

    async postToGroup(groupId, text) {
        try {
            const page = await this.browser.newPage();
            await page.setViewport({ width: 1280, height: 800 })
            await page.goto(getGroupLinkById(groupId))

            await page.click(selector.POST_TEXTAREA_SELECTOR);
            await page.keyboard.type(text)

            await delay(3000)

            await page.click(selector.POST_BUTTON_SELECTOR)

            await delay(6000)

            return true
        } catch (err) {
            console.log(`Posting error ${err}`)
            return false
        }
    }

    async checkLoginStatus() {
        const page = await this.browser.newPage();
        await page.setViewport({ width: 1280, height: 800 })

        await page.goto(selector.privatePageURL)

        const pageUrl = this.page.url()
        if (pageUrl == selector.privatePageURL) {
            return true
        } else return false
    }
    async setCookie() {
        const cookiesArr = myCookies
        for (let cookie of cookiesArr) {
            await this.page.setCookie(cookie)
        }
    }
}

export default Facebook

function getGroupLinkById(groupId) {
    return `${selector.groopHalfURL}/${groupId}/`
}
