'use strict'

import mongoose from 'mongoose'

import logger from '../models/logger'
import config from '../config/creds'

import * as post from './post'

export { post }

mongoose.Promise = global.Promise
mongoose.connect(config.databaseURL, {
        useNewUrlParser: true
    })
    .catch(e => {
        logger.error('Mongo Error->' + e)
    })

const db = mongoose.connection
