'use strict'

import mongoose from 'mongoose'
import mongoose_validator from 'mongoose-validator'
import config from '../config/creds'


const dataSchema = mongoose.Schema({
    published: { type: Boolean, default: false },
    text: {
        type: String,
        required: true,
    },
    groupLink: {
        type: String,
        required: true,
    },
    createDate: {
        type: Date,
        default: Date.now,
        set: (val) => this.securedField,
        auto: true
    },
    publishDate: {
        type: Date,
        required: true
    },
})

const Model = mongoose.model('posts', dataSchema)

export default Model

export async function byId(id) {
    return Model.findOne({ "_id": id })
}

export async function add(data) {
    data.published = false
    data.createDate = new Date()
    return Model.collection.insertOne(data)
}

export async function getAll() {
    return Model.find({ published: false })
        .sort({ 'createDate': -1 })
}
