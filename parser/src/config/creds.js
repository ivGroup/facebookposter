'use strict'

const creds = {}

creds.login = process.env.login
creds.pass = process.env.pass
creds.databaseURL = process.env.databaseURL
creds.status = process.env.status || 'prod'
export default creds
