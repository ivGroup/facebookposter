'use strict'

import 'babel-polyfill'

import dateLib from 'date-and-time'

import Parser from './models/parser'
import * as dbController from './db/dbController'

import logger from './models/logger'
import Worker from './models/worker'

import creds from './config/creds'

const SMALL_LOOP_INTERVAL_SEC = 10 // less 60
const BIG_LOOP_INTERVAL_SEC = 60 // less 60*10
const BIG_LOOP_INTERVAL_MIN = Math.round(BIG_LOOP_INTERVAL_SEC / 60)

let parser = null
let delayPostHolder = {}

start()

async function start() {
    parser = new Parser(creds.login, creds.pass)
    const loginResult = await parser.login()
    if (!loginResult) {
        return
    }
    console.log('You are logged In and ready to work')
    BIG_LOOP.startJob()
    SMALL_LOOP.startJob()
}

const SMALL_LOOP = new Worker(`*/${SMALL_LOOP_INTERVAL_SEC} * * * * *`, () => {
    postPostes()
})

const BIG_LOOP = new Worker(`* */${BIG_LOOP_INTERVAL_MIN} * * *`, () => {
    getAllPosts()
})

async function postPostes() {
    console.log(Object.values(delayPostHolder))
    const postToPublishNow = Object.values(delayPostHolder)
        .map(post => {
            return publishPost(post)
        })
    delayPostHolder = []
    Promise.all(postToPublishNow)
        .then(result => {

        })
        .catch(err => {
            logger.error(err.toString())
        })
}

async function getAllPosts() {

    const deferredPost = await dbController.post.getAll()
    for (const post of deferredPost) {
        const delay = getPostDelay(post.publishDate)
        console.log(delay)
        if (delay < 0) {
            logger.debug('Dismiss Post Time!')
            await markPostAsPublished(post)
        } else if (delay < BIG_LOOP_INTERVAL_SEC) {
            const key = `${post.text}:${post.groupLink}:${post.publishDate}`
            if (!delayPostHolder[key])
                delayPostHolder[key] = post
        }
    }
}

function getPostDelay(publishDate) {
    const publishDateInMils = new Date(publishDate)
        .getTime()
    const nowDateInMils = new Date()
        .getTime()
    const delay = Math.round((publishDateInMils - nowDateInMils) / 1000)
    return delay
}

async function publishPost(post) {
    const postResult = await parser.postToGroup(post.groupLink, post.text)
    await markPostAsPublished(post)
}
async function markPostAsPublished(post) {
    post.published = true
    await post.save()
}
