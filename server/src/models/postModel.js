'use strict'

import * as dbController from '../db/dbController'
import logger from './logger'


export const addPost = async (text, groupLink, publishDate) => {
    const newPost = { text, groupLink, publishDate }
    const result = await dbController.post.add(newPost)
    return true
}

export const getAllPost = async () => {
    try {
        const posts = await dbController.post.getAll()
        return posts
    } catch (err) {
        return undefined
    }
}
