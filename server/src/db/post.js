'use strict'

import mongoose from 'mongoose'
import mongoose_validator from 'mongoose-validator'
import config from '../appConfig'

const { maxPostCharactersCount, maxgroupLinkCharactersCount } = config.FACEBOOK

const textValidator = [
    mongoose_validator({
        validator: 'isLength',
        arguments: [1, maxPostCharactersCount],
        message: 'Name should be between {ARGS[0]} and {ARGS[1]} characters',
    })
]

const groupLinkValidator = [
    mongoose_validator({
        validator: 'isLength',
        arguments: [1, maxgroupLinkCharactersCount],
        message: 'Name should be between {ARGS[0]} and {ARGS[1]} characters',
    })
]

const dataSchema = mongoose.Schema({
    published: { type: Boolean, default: false },
    text: {
        type: String,
        required: true,
        validate: textValidator
    },
    groupLink: {
        type: String,
        required: true,
        validate: groupLinkValidator
    },
    createDate: {
        type: Date,
        default: Date.now,
        set: (val) => this.securedField,
        auto: true
    },
    publishDate: {
        type: Date,
        required: true
    },
})

const Model = mongoose.model('posts', dataSchema)

export default Model

export async function byId(id) {
    return Model.findOne({ "_id": id })
}

export async function add(data) {
    data.published = false
    data.createDate = new Date()
    return Model.collection.insertOne(data)
}

export async function getAll() {
    return Model.find({ published: false })
        .sort({ 'createDate': -1 })
}
