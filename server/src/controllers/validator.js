'use strict'

import { body, validationResult, param, query } from 'express-validator/check'
import logger from '../models/logger'
import config from '../appConfig'

const { maxPostCharactersCount, maxgroupLinkCharactersCount } = config.FACEBOOK

const getAllPostValidator = () => {
    return []
}

const addPostValidator = () => {
    return [
        query('text', 'text doesn`t exists')
        .isLength({ min: 1, max: maxPostCharactersCount })
        .exists(),
        query('groupLink', 'groupLink doesn`t exists')
        .isLength({ min: 1, max: maxgroupLinkCharactersCount })
        .exists(),
        query('publishDate', 'date doesn`t exists')
        .isLength({ min: 1, max: 30 })
        .exists()
    ]
}

export const validate = (validationMethod) => {
    switch (validationMethod) {
    case 'addPost': {
        return addPostValidator()
    }
    case 'getAllPost': {
        return getAllPostValidator()
    }
    }
}

export const validationHandler = (req, res, next) => {
    const validationError = validationResult(req)
    if (!validationError.isEmpty()) {
        console.log(validationError.array())
        const httpError = new Error(`Invalid input`)
        httpError.status = 400
        throw httpError
    } else next()
}
