'use strict'

import { validationResult } from 'express-validator/check'

import * as postModel from '../models/postModel'
import logger from '../models/logger'
import { post } from '../db/dbController';

export const addPost = async (req, res, next) => {
    const { text, groupLink, publishDate } = req.query
    postModel.addPost(text, groupLink, publishDate)
        .then(newlyInserted => {
            sendSuccess(res)
        })
        .catch(err => {
            logger.error(err)
            next(new Error('Unexpected Error'))
        })
}

export const getAllPost = async (req, res, next) => {
    const posts = await postModel.getAllPost()
    if (posts) {
        sendSuccess(res, posts)
    } else {
        const httpError = new Error(`Unexpected Error`)
        httpError.status = 404
        next(httpError)
    }
}

function sendSuccess(res, object) {
    object = object || { description: 'successful operation' }
    res
        .status(200)
        .json(object)
}
