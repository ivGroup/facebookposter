'use strict'

const config = {}

config.port = process.env.PORT || 5000
config.status = process.env.status || 'prod'
config.databaseURL = process.env.databaseURL

config.FACEBOOK = {}
config.FACEBOOK.maxPostCharactersCount = 1000
config.FACEBOOK.maxgroupLinkCharactersCount = 100
export default config
