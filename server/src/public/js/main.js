var socket = io.connect('http://localhost:5000')
socket.on('connect', function (data) {
    socket.emit('join', 'Hello World from client')
});

$('#publish')
    .click(function () {
        var groupLink = $('#groupLink')
            .val() || ''
        var text = $('#text')
            .val() || ''
        var publishDate = $("#publishDate")
            .find("input")
            .val() || ''
        publishDate = publishDate + ''
        console.log(publishDate)
        // svar postData = { groupLink: groupLink, text: text, publishDate: publishDate }
        var queryString = "?groupLink=" + groupLink + "&text=" + text + "&publishDate=" + publishDate
        $.post("api/v1/post" + queryString, function (data) {
                alert('Запрос прошел успешно')
                reloadView()
            })
            .fail(function () {
                alert("Что-то не так с данными");
            })
    })

function reloadView() {
    $.get("api/v1/post/list", function (data) {
            var content = data.map(function (i) {
                    return '<div><h4>Group Link: ' + i.groupLink + '</h4><p>Post text: ' + i.text + '</p></div><p>Post date: ' + i.publishDate + '</p></div>'
                })
                .join('')
            $('#messages')
                .html(content)
        })
        .fail(function () {})
}
$(function () {
    $('#publishDate')
        .datetimepicker({ defaultDate: new Date() });
});
