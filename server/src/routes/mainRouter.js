import { Router } from 'express'

import * as userController from '../controllers/postController'
import { validate, validationHandler } from '../controllers/validator'
const router = Router()

router.post('/post',
    validate('addPost'),
    validationHandler,
    userController.addPost)

router.get('/post/list',
    validate('getAllPost'),
    validationHandler,
    userController.getAllPost)

export default router
