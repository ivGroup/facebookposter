import { Router } from 'express'

import * as userController from '../controllers/postController'
import { getAllPost } from '../models/postModel'

const router = Router()

router.get('/', async (req, res, next) => {
    const allPost = await getAllPost()
    res.render('main', { posts: allPost })
})

export default router
