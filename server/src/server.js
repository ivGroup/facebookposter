'use strict'

import 'babel-polyfill'

import logger from './models/logger'

import makeApp, { startApp } from './makeApp'


startApp()
    .catch(err => {
        logger.error(`Error in express middelware: ${err}`)
    })
